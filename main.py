#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import logging
import math

from robot_game.robot import Robot
from robot_game.board import Board
from robot_game.actions.place import Place
from robot_game.actions.move import Move
from robot_game.actions.rotate import Rotate
from robot_game.actions.report import Report

parser = argparse.ArgumentParser(description="Robot")
parser.add_argument("-v", "--verbose", help="Show info of the process",
                    action="count", default=0)
parser.add_argument("-f", "--file", help="File of actions")
parser.add_argument("-i", "--interactive", action="store_true",
                    help="Play interactively")

args = parser.parse_args()

if args.verbose == 0:
    logging.getLogger().setLevel(logging.WARNING)
elif args.verbose == 1:
    logging.getLogger().setLevel(logging.INFO)
else:
    logging.getLogger().setLevel(logging.DEBUG)

board = Board(5, 5)
actions_dict = {
    'REPORT': Report(),
    'PLACE': Place(),
    'MOVE': Move(),
    'RIGHT': Rotate(-math.pi/2),
    'LEFT': Rotate(math.pi/2)
}
rob = Robot(board, actions_dict)
if args.file:
    rob.perform_file_actions(args.file)
elif args.interactive:
    while True:
        try:
            action = input()
        except (EOFError, KeyboardInterrupt):
            action = "QUIT"

        if action == "QUIT":
            break
        else:
            rob.perform_action(action)
