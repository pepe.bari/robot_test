
class Move():
    def __init__(self, units=1):
        self.units = units

    def perform_action(self, robot, action_string):
        if robot.status.position is None:
            return

        # applying translation of 1 unit, if possible
        new_position = robot.status.position + \
            self.units * robot.status.direction
        if robot.board.check_position(new_position):
            robot.status.update(new_position)
