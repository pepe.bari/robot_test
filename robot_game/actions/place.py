import logging
import numpy as np


class Place():
    def perform_action(self, robot, action_string):
        # PLACE command requires parameters
        try:
            params = action_string.split(" ")[1]
            x = int(params.split(",")[0])
            y = int(params.split(",")[1])
            orientation = str(params.split(",")[2])
        except Exception:
            logging.info("Error parsing parameters of PLACE command")
            return

        # transform coordinates to 2d point
        position = np.array([x, y])

        if not robot.board.check_position(position):
            # Initial position not valid
            logging.info("Initial position not valid")
            return

        if orientation not in robot.status.orientation_direction:
            # orientation not valid
            logging.info("Initial orientation not valid")
            return

        # at this point, the position, orientation and direction can be stored
        robot.status.update(position, orientation)
