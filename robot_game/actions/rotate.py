import numpy as np


class Rotate():
    def __init__(self, angle):
        self.angle = angle

    def perform_action(self, robot, action_string):
        if robot.status.position is None:
            return

        # 2d rotation matrix
        # ( cos(angle)  -sin(angle) )
        # ( sin(angle)   cos(angle) )
        c = round(np.cos(self.angle))
        s = round(np.sin(self.angle))
        rot_mat = np.array(((c, -s), (s, c)))

        # applying rotation
        new_direction = rot_mat.dot(robot.status.direction)
        robot.status.update_direction(new_direction)
