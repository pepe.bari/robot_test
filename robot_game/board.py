
class Board():
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def check_position(self, position):
        x = position[0]
        y = position[1]

        if x < 0 or y < 0:
            return False

        if x >= self.width:
            return False

        if y >= self.height:
            return False

        return True
