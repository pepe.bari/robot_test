# -*- coding: utf-8 -*-
import logging

from .status import Status


class Robot():
    def __init__(self, board, actions_dict):
        self.board = board
        self.actions_dict = actions_dict
        self.status = Status()

    def perform_action(self, action_string):
        # the first part
        action = action_string.split(" ")[0]
        if action in self.actions_dict:
            self.actions_dict[action].perform_action(self, action_string)
        else:
            logging.info("Action unknown")

    def perform_file_actions(self, file):
        with open(file, "r") as f:
            # reading file line by line
            for line in f.readlines():
                # remove trailing newlines
                line = line.rstrip()
                self.perform_action(line)
