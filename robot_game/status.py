import numpy as np


class Status():
    def __init__(self):
        # The position will be None until a successful place is called
        self.position = None

        # dict to translate cardinal point to 2D vectors
        # this way will be easier to perform translations and rotations
        self.orientation_direction = {
            'NORTH': np.array([0, 1]),
            'SOUTH': np.array([0, -1]),
            'EAST': np.array([1, 0]),
            'WEST': np.array([-1, 0])
        }

    def update(self, position, orientation=None):
        self.position = position
        if orientation:
            self.orientation = orientation
            self.direction = self.orientation_direction[self.orientation]

    def update_direction(self, direction):
        self.direction = direction
        # seek which orientation corresponds to this direction
        for orientation, direction in self.orientation_direction.items():
            if (direction == self.direction).all():
                self.orientation = orientation

    def __str__(self):
        if self.position is None:
            return 'not in place'

        x = str(self.position[0])
        y = str(self.position[1])
        return x + ',' + y + ',' + self.orientation
