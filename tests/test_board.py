# -*- coding: utf-8 -*-

import numpy as np

import unittest

from robot_game.board import Board


class TestBoard(unittest.TestCase):

    def test_1(self):
        b1 = Board(5, 5)

        for i in range(5):
            for j in range(5):
                self.assertTrue(b1.check_position(np.array([i, j])))

        self.assertFalse(b1.check_position(np.array([-1, -1])))
        self.assertFalse(b1.check_position(np.array([0, 5])))
        self.assertFalse(b1.check_position(np.array([10, 2])))
