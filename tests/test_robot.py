# -*- coding: utf-8 -*-
import math
import unittest

from robot_game.robot import Robot
from robot_game.board import Board
from robot_game.actions.place import Place
from robot_game.actions.move import Move
from robot_game.actions.rotate import Rotate
from robot_game.actions.report import Report

board = Board(5, 5)
actions_dict = {
    'REPORT': Report(),
    'PLACE': Place(),
    'MOVE': Move(),
    'RIGHT': Rotate(-math.pi/2),
    'LEFT': Rotate(math.pi/2)
}


class TestRobot(unittest.TestCase):

    def test_1(self):
        """TEST 1: ignores commands before robot is put into place

        command list:
        MOVE
        LEFT
        RIGHT
        REPORT

        result: output will be 'not in place', showing that the commands had no effect."""

        r1 = Robot(board, actions_dict)
        r1.perform_action("MOVE")
        r1.perform_action("LEFT")
        r1.perform_action("RIGHT")

        self.assertEqual(r1.status.__str__(), "not in place")

    def test_2(self):
        """TEST 2: place robot on a given position
        commands:
        PLACE 1,2,NORTH
        REPORT

        expected result: '1,2,NORTH', robot is in the correct position"""

        r2 = Robot(board, actions_dict)
        r2.perform_action("PLACE 1,2,NORTH")

        self.assertEqual(r2.status.__str__(), '1,2,NORTH')

    def test_3(self):
        """TEST 3: robot is not placed in an invalid position
        commands:
        PLACE 9,9,NORTH
        REPORT

        expected result: 'not in place', invalid position was ignored"""

        r3 = Robot(board, actions_dict)
        r3.perform_action("PLACE 9,9,NORTH")

        self.assertEqual(r3.status.__str__(), "not in place")

    def test_4(self):
        """TEST 4: robot rotates right
        commands:
        PLACE 1,1,NORTH
        RIGHT
        REPORT
        RIGHT
        REPORT
        RIGHT
        REPORT
        RIGHT
        REPORT

        expected result: output will show the robot facing East, South, West and North again, sequentially."""

        output = ""
        r4 = Robot(board, actions_dict)
        r4.perform_action("PLACE 1,1,NORTH")
        r4.perform_action("RIGHT")
        output += r4.status.orientation
        r4.perform_action("RIGHT")
        output += r4.status.orientation
        r4.perform_action("RIGHT")
        output += r4.status.orientation
        r4.perform_action("RIGHT")
        output += r4.status.orientation

        self.assertEqual(output, "EASTSOUTHWESTNORTH")

    def test_5(self):
        """TEST 5: robot rotates left
        PLACE 1,1,NORTH
        LEFT
        REPORT
        LEFT
        REPORT
        LEFT
        REPORT
        LEFT
        REPORT

        expected result: output will show the robot facing West, South, East and North again, sequentially"""

        output = ""
        r5 = Robot(board, actions_dict)
        r5.perform_action("PLACE 1,1,NORTH")
        r5.perform_action("LEFT")
        self.assertEqual(r5.status.__str__(), "1,1,WEST")
        r5.perform_action("LEFT")
        self.assertEqual(r5.status.__str__(), "1,1,SOUTH")
        r5.perform_action("LEFT")
        self.assertEqual(r5.status.__str__(), "1,1,EAST")
        r5.perform_action("LEFT")
        self.assertEqual(r5.status.__str__(), "1,1,NORTH")

    def test_6(self):
        """TEST 6: robot is able to move in all four cardinal directions
        PLACE 1,1,NORTH
        MOVE
        RIGHT
        REPORT
        MOVE
        RIGHT
        REPORT
        MOVE
        RIGHT
        REPORT
        MOVE
        RIGHT
        REPORT

        expected result: output will be:

        1,2,EAST
        2,2,SOUTH
        2,1,WEST
        1,1,NORTH

        showing that the robot walked 1 unit north, 1 unit east, 1 unit south and 1 unit west, making a lap and ending up at the beginning"""

        r6 = Robot(board, actions_dict)
        r6.perform_action("PLACE 1,1,NORTH")
        r6.perform_action("MOVE")
        r6.perform_action("RIGHT")
        self.assertEqual(r6.status.__str__(), "1,2,EAST")
        r6.perform_action("MOVE")
        r6.perform_action("RIGHT")
        self.assertEqual(r6.status.__str__(), "2,2,SOUTH")
        r6.perform_action("MOVE")
        r6.perform_action("RIGHT")
        self.assertEqual(r6.status.__str__(), "2,1,WEST")
        r6.perform_action("MOVE")
        r6.perform_action("RIGHT")
        self.assertEqual(r6.status.__str__(), "1,1,NORTH")
